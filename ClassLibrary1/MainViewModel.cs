﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using ClassLibrary1.Annotations;
using MvvmCross.Core.ViewModels;

namespace ClassLibrary1
{
    public class MainViewModel : MvxViewModel
    {
        private readonly IAlertDisplayer _alertDisplayer;
        private string _login;
        private string _password;

        public MainViewModel(IAlertDisplayer alertDisplayer)
        {
            _alertDisplayer = alertDisplayer;
        }
        
        public string Login
        {
            get => _login;
            set => SetProperty(ref _login, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public ICommand VerifyDataCommand => new Command(() =>
        {
            if (Login == "a" && Password == "b")
            {
                _alertDisplayer.DisplayAlert("Dane poprawne");
            }
            else
            {
                _alertDisplayer.DisplayAlert("Dane błędne");
            }
        });

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class Command : ICommand
    {
        private readonly Action _action;

        public Command(Action action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action.Invoke();
        }

        public event EventHandler CanExecuteChanged;
    }

    public interface IAlertDisplayer
    {
        void DisplayAlert(string message);
    }
}
