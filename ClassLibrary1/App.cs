using MvvmCross.Platform;
using MvvmCross.Platform.IoC;

namespace ClassLibrary1
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
            
            RegisterAppStart<MainViewModel>();
        }
    }
}
