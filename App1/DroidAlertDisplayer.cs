﻿using Android.App;
using ClassLibrary1;
using MvvmCross.Platform.Droid.Platform;

namespace App1
{
    public class DroidAlertDisplayer : IAlertDisplayer
    {
        private readonly IMvxAndroidCurrentTopActivity _currentTopActivity;

        public DroidAlertDisplayer(IMvxAndroidCurrentTopActivity currentTopActivity)
        {
            _currentTopActivity = currentTopActivity;
        }

        public void DisplayAlert(string message)
        {
            new AlertDialog.Builder(_currentTopActivity.Activity)
                .SetMessage(message)
                .SetNeutralButton("OK", (sender, args) => {})
                .Create()
                .Show();
        }
    }
}