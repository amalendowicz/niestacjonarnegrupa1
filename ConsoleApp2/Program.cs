﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace ConsoleApp2
{
    class Program
    {
        private static MainViewModel _mainViewModel;

        static void Main(string[] args)
        {
            _mainViewModel = new MainViewModel(new ConsoleAlertDisplayer());

            Console.WriteLine("Podaj login");
            var login = Console.ReadLine();

            Console.WriteLine("Podaj hasło");
            var password = Console.ReadLine();

            //_mainViewModel.VerifyLoginData(login, password);

            Console.ReadKey();
        }

        
    }

    internal class ConsoleAlertDisplayer : IAlertDisplayer
    {
        public void DisplayAlert(string message)
        {
            Console.WriteLine(message);
        }
    }
}
