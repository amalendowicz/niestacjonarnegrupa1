﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private MainViewModel _mainViewModel;

        public Form1()
        {
            InitializeComponent();
            _mainViewModel = new MainViewModel(new FormsAlertDisplayer());
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        
    }

    public class FormsAlertDisplayer : IAlertDisplayer
    {
        public void DisplayAlert(string message)
        {
            MessageBox.Show(message);
        }
    }
}
